﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Private_place.aspx.cs" Inherits="Student.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">





    <div class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="hpanel">
                    <div class="panel-body">
                        <h1>Private area</h1>
                        <p class="lead">
                           It's a private area of our website which helps you to update your profile and to have a look on the list of the lessons which comes with your course
                        </p>

                           <small><asp:Label ID="LabelMessage" runat="server" Text="Label"></asp:Label></small><br /><br />
                    <asp:Button class="btn btn-outline btn-primary" ID="ButtonLog_out" runat="server"  Text="Log out" OnClick="ButtonLog_out_Click" />
                    
              


 <asp:Panel ID="PanelStudent" runat="server">

    <div class="row">
        <div class="col-lg-6">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                      
                    </div>
                    Student update form
                </div>
                <div class="panel-body">
           


<div class="register-container">
    <div class="row">
        <div class="col-md-12">
 
            <div class="hpanel">
         
                        <form action="#" id="loginForm">
                            <div class="row">
                            
                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelCourse" runat="server" Text="Course"></asp:Label>
                           <asp:DropDownList class="form-control" ID="DropDownCourse" Name="DropDownCourse"  runat="server"  OnTextChanged="DropDownCourse_TextChanged" AutoPostBack="True"></asp:DropDownList>
                           </div>



                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelName" runat="server" Text="Name"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxName" Name="TextBoxName" runat="server" ></asp:TextBox>
                            <asp:Label ID="LabelWarningName" runat="server" Text="Field [Name] can not be empty"></asp:Label>
                            </div>

                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelSurname" runat="server" Text="Surname"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxSurname" Name="TextBoxSurname" runat="server" ></asp:TextBox>
                            <asp:Label ID="LabelWarningSurname" runat="server" Text="Field [Surname] can not be empty"></asp:Label>
                            </div>

                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelMobile_phone" runat="server" Text="Mobile phone"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxMobile_phone" Name="TextBoxMobile_phone" runat="server" ></asp:TextBox>
                            </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelSuburb" runat="server" Text="Suburb"></asp:Label>
                           <asp:DropDownList class="form-control" ID="DropDownSuburb" Name="DropDownSuburb" runat="server"></asp:DropDownList>
                           </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelStreet" runat="server" Text="Street"></asp:Label>
                           <asp:TextBox class="form-control" ID="TextBoxStreet" Name="TextBoxStreet" runat="server" ></asp:TextBox>
                           </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelHouse" runat="server" Text="House"></asp:Label>
                           <asp:TextBox class="form-control" ID="TextBoxHouse" Name="TextBoxHouse" runat="server" ></asp:TextBox>
                           </div>


                        

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelGender" runat="server" Text="Gender"></asp:Label>
                           <asp:DropDownList class="form-control" ID="DropDownGender" Name="DropDownGender"  runat="server"></asp:DropDownList>
                           </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelDate_of_birth" runat="server" Text="Date of Birth"></asp:Label>
                           <asp:TextBox class="form-control" ID="txtDate_of_birth2" runat="server" Name="txtDate_of_birth2"  ReadOnly = "true"></asp:TextBox>
                           </div>

                            </div>
<asp:Label ID="labelMessage1" runat="server" Text=""></asp:Label>
                            <div class="text-center">
                                <asp:Button class="btn btn-success" ID="ButtonUpdate" runat="server" OnClick="ButtonUpdate_Click" Text="Update" />
                                  <asp:Button class="btn btn-primary" ID="ButtonPasswordChange" runat="server"  Text="Password change" OnClick="ButtonPasswordChange_Click" />
                                
                            </div>
                        </form>
               
            </div>




        </div>
    </div>

</div>



                   

                  
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="hpanel">
                <div class="panel-heading">
                    <div class="panel-tools">
                  
                    </div>
                    Lessons of defined course
                </div>
                <div class="panel-body">
                 <div class="col-sm-12">
                <asp:GridView class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="example2_info" ID="GridView2" runat="server">
                </asp:GridView>
                </div>
            
                </div>
            </div>
        </div>

    </div>
  
     </asp:Panel>

 
  <br>  <br>

<asp:Panel ID="PanelAdministrator" runat="server">

<div class="hpanel">
<div class="panel-body">
<div class="col-sm-12">
<asp:GridView ID="GridViewStudent" class="table table-striped table-bordered table-hover dataTable no-footer" runat="server" AutoGenerateColumns="False" >
<Columns>
<asp:BoundField DataField="student_id" HeaderText ="ID" />
<asp:BoundField DataField="name" HeaderText ="Name11" />
<asp:BoundField DataField="surname" HeaderText ="Surname" />
<asp:BoundField DataField="email" HeaderText ="Email" />
<asp:BoundField DataField="mobile_phone" HeaderText ="Mobile phone" />
<asp:BoundField DataField="suburb" HeaderText ="Suburb" />
<asp:BoundField DataField="street" HeaderText ="Street" />
<asp:BoundField DataField="house" HeaderText ="House" />
<asp:BoundField DataField="course" HeaderText ="Course" />
<asp:BoundField DataField="date_of_birth" HeaderText ="Date of birth" />
<asp:BoundField DataField="password" HeaderText ="Password" />
<asp:BoundField DataField="gender" HeaderText ="Gender" />
<asp:BoundField DataField="approved" HeaderText ="Approved" />
<asp:TemplateField HeaderText="Delete action">
<ItemTemplate>
<asp:LinkButton ID="lnkSelect" Text="Delete student" runat="server" CommandArgument='<%# Eval("student_id") %>' OnClick="lnkSelect_Click"/>
</ItemTemplate>
</asp:TemplateField>
</Columns>
</asp:GridView>


                       </div>
                </div>
            </div>
        </div>





    </div>


</div>
</div>
</div>






<div class="hpanel">
<div class="panel-body">
<div class="col-sm-12">
<asp:GridView class="table table-striped table-bordered table-hover dataTable no-footer" role="grid" aria-describedby="example2_info" ID="GridView1" runat="server"  Visible="False">
</asp:GridView>
</div>
</div>
</div>










</asp:Panel>



</div>
</form>
</body>
</html>





</asp:Content>

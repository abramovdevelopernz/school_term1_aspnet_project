﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net.Mail;


//using System.Data.Datatable;
using System.Text.RegularExpressions;

namespace Student
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader dr;
        private Int32 pEmpty;

       
        protected void Page_init(object sender, EventArgs e)
        {
            LabelWarningEmail.Visible = false;
            LabelWarningPassword.Visible = false;
            LabelBadPassword.Visible = false;
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void ButtonSignIn_Click(object sender, EventArgs e)
        {

            pEmpty = 0;
            LabelWarningEmail.Visible = false;
            LabelWarningPassword.Visible = false;

            if (TextBoxEmail.Text.Trim() == "")

            {
                LabelWarningEmail.Visible = true;
                LabelWarningEmail.Text = "Field [Email] can not be empty";
                //TextBoxEmailNew.Focus();
                //return;
                pEmpty = 1;

            }

            Regex emailRegex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");

            if (emailRegex.IsMatch(TextBoxEmail.Text.Trim()) == false)
            {
                LabelWarningEmail.Visible = true;
                LabelWarningEmail.Text = "Email field is in a wrong format";
                //TextBoxEmailNew.Focus();
                //return;
            }

            //LabelWarningEmail.Visible = false;

            if (string.IsNullOrEmpty(TextBoxPassword.Text.Trim()))
            {
                LabelWarningPassword.Visible = true;
                LabelWarningPassword.Text = "Field [Password] can not be empty";
                //TextBoxPassword.Focus();
                //return;
                pEmpty = 1;
            }


         
            if (pEmpty == 1)
            {

                return;
            }


            LabelWarningEmail.Visible = false;
            LabelWarningPassword.Visible = false;
          
            conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("SELECT * from student where email = '" + TextBoxEmail.Text + "' and password = '"+TextBoxPassword.Text+"'", conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);

            pEmpty = 0;

            if (dt.Rows.Count > 0)
            {
                Session["current_id"] = dt.Rows[0]["id"].ToString();
                Session["current_role_id"] = dt.Rows[0]["role_id"].ToString();
                Session["current_full_name"] = dt.Rows[0]["name"].ToString()+""+ dt.Rows[0]["surname"].ToString();
                Response.Redirect("Private_place.aspx");
            }
            else
            {
                LabelBadPassword.Visible = true;
            }
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Registration.aspx");
        }

        protected void ButtonRegister_Click(object sender, EventArgs e)
        {
            Response.Redirect("Registration.aspx");
        }

        protected void ButtonEmail_Click(object sender, EventArgs e)
        {

        }
    }

}
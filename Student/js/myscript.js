$(function () {
	
});

$(document).ready(function(){
	
	var idfiledata = 0;
	
	$('.load_document_window').on('click', '.fileupload', function (e) {	
    idfiledata = $(this).attr('id-file-data');
	idzayavka = $(this).attr('id-zayavka');
	'use strict';
    var url = window.location.hostname === 'blueimp.github.io' ? '//b.1b.kz/' : 'tmp/doc/';
		
	//https://blueimp.github.io/jQuery-File-Upload/basic-plus.html		
    $(this).fileupload({
        url: url,
        dataType: 'json',
        acceptFileTypes: /(\.|\/)(zip|rar|pdf)$/i,
		
	//	 maxChunkSize: 5000000, // 5 MB
	//	maxFileSize: 20000000, // 20 MB
		
		
     // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
         /*
            // The regular expression for allowed file types, matches
            // against either file type or file name:
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
            // The maximum allowed file size in bytes:
            maxFileSize: 10000000, // 10 MB
            // The minimum allowed file size in bytes:
            minFileSize: undefined, // No minimal file size
            // The limit of files to be uploaded:
            maxNumberOfFiles: 10,

			maxFileSize: 5000000, // Maximum File Size in Bytes - 5 MB
            minFileSize: 100000, // Minimum File Size in Bytes - 100 KB
            acceptFileTypes: /(zip)|(rar)$/i  // Allowed File Types
			
            */		

		formData: [{ name: 'pUploadFileID', value: idfiledata }],

        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo('#files');
            });
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            
			
			
			$(this).parent().parent().children('#progress').children('.progress-bar').css(
                'width',
                progress + '%'
            );
			
			
			if (data.loaded==data.total) 
			{
				
				setTimeout(function(){ 
				  $('.load_document_window').load('/ajax/list_documents.php', {zayavka_id:idzayavka}, function() {
                  });
				  swal("Отправлен!", "Ваш файл успешно отправлен", "success");
				}, 3000);

				};

        }
		
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	
		
});



//Поиск заказчика
$('.zak_search').click(function(e){
	e.preventDefault();
	iin_bin_zak = $(this).parent().parent().children('#iin_bin_zak').val();	
	
//	alert(iin_bin_zak);	
	
	if ( iin_bin_zak == "" )
	{
		
		swal("Внимание", "Необходимо указать ИИН/БИН заказчика", "error")
		} 
		else 
		{
	
	
			if ( iin_bin_zak.length != 12 ){
				swal("Внимание", "ИИН/БИН заказчика должен содержать 12 цифр", "error")
			} 
			else 
			{
		
		
			//   $(this).parent().parent().children('#iin_bin_zak').css("border", "5px solid #F00");
				$('#zak_search').load('/ajax/zak_search.php', {iin_bin_zak:iin_bin_zak}, function() {
				});		
				
			}

	

		}
});



	

	$('.cifri').on('keyup keypress', function(e) {
		   if (e.keyCode == 8 || e.keyCode == 46) {

			   }
		   else
			 {
			   var letters='1234567890.';
			   //if (e.length === 12){alert("Превышение")};	
							return (letters.indexOf(String.fromCharCode(e.which))!=-1);
						};
					
    });
	
	
	$('.filter_rayon').on('change', '.get_oblast_id', function(e){
			var ident = jQuery(this).val();
			//$('#rayon').load('/ajax/filter_rayon.php', {ident:ident}, function() {});
			$('.rayon').load('/ajax/filter_rayon.php', {ident:ident}, function() {});
			

	});
	

	
$('.zayavka_table').on('click', '.zagruzka_dokumentov_window', function (e) {	
	
//$(".load_dokument").on('click','.zagruzka_dokumentov_window', function(e) {
	zayavka_id = $(this).attr('id');
    $('.load_document_window').load('/ajax/list_documents.php', {zayavka_id:zayavka_id}, function() {
	  $('#zagruzka_dokumentov_window').modal('show');
	});
});




$('.zayavka_table').on('click', '.update_zayavka_record', function (e) {	
	zayavka_id = $(this).attr('id');
	
	   swal({
                        title: "Вы уверены?",
                        text: "Желаете продолжить редактирование заявки!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#008000",
						cancelButtonText: "Нет, не буду!",
                        confirmButtonText: "Да, продолжаю!"
                    },
                        function (isConfirm) {
                        if (isConfirm) {
							$('.edit_zayavka_window').load('/ajax/edit_zayavka_id.php', {zayavka_id:zayavka_id}, function() {
										
		$(".update_zayavka").validate({
		submitHandler: function(form) {
			
			var m_data=$('.update_zayavka').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/update_zayavka.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.update_zayavka').slideUp(1000);
					
					$('#uspeh').html(result.message);
					$('.zayavka_table').load('/ajax/reload_list_zayavka.php',  function() {});
				}
				else
				{
					$('#uspeh').html(result.message);
				}
			}
			});

		},
       rules:{

         	klass_opasnosti:{
				required: true,
            },
			
			na_kakie_goda_zayavka:{
				required: true,
            },
			
			contact:{
				required: true,
            },			

       },
	   
       messages:{

			klass_opasnosti:{
                required:  "Выберите класс опасности",
            }, 

			na_kakie_goda_zayavka:{
                required:  "Укажите на какие года подается заявка",
            }, 

			contact:{
                required:  "Укажите контактное лицо и номер телефона",
            }, 			

       }
	   
		
    });	
								
								
							$('#update_zayavka_record').modal('show');
							});
                        } else {
                           // swal("2", "Ваша заявка цела и невредима :)", "error");
                        }
                    });
});

				
	$( "#datepicker1" ).datepicker({
	  dateFormat: 'dd.mm.yy',
      changeMonth: true,
      changeYear: true
    });
				
				
				
	$( "#datepicker2" ).datepicker({
	  dateFormat: 'dd.mm.yy',
      changeMonth: true,
      changeYear: true
    });
	
  $('.input-group.date').datepicker();

        $("#demo1").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10
        });

        $("#demo2").TouchSpin({
            verticalbuttons: true
        });

        $(".select2").select2()	
	
	
	
	$('.search_clear').click(function(e){
		e.preventDefault();
		$('#search_zayavka_temp')[0].reset();
	});
	
	
	
	$('.search_zayavka').click(function(e){
		
		e.preventDefault();

		var check_zakazchik_nazvanie_predpriyatiya_rus = $('.check_zakazchik_nazvanie_predpriyatiya_rus:checked').val();
		var check_zakazchik_iin_bin = $('.check_zakazchik_iin_bin:checked').val();
		var check_status_zayavki = $('.check_status_zayavki:checked').val();

		if (check_zakazchik_nazvanie_predpriyatiya_rus == 1)
		{
			var zakazchik_nazvanie_predpriyatiya_rus = $('.zakazchik_nazvanie_predpriyatiya_rus').val();
		};
		
		if (check_zakazchik_iin_bin == 1)
		{
			var zakazchik_iin_bin = $('.zakazchik_iin_bin').val();
		};		
		
		
		if (check_status_zayavki == 1)
		{
			var status_zayavki = $('.status_zayavki').val();
		};			

		window.location = "/?page=list_zayavka&status_zayavki_id=0&zakazchik_nazvanie_predpriyatiya_rus"+zakazchik_nazvanie_predpriyatiya_rus+"&zakazchik_iin_bin"+zakazchik_iin_bin+"&status_zayavki"+status_zayavki+"&"
	});

$('.load_document_window').on('click', '.post_msg_zametki2', function () {
			document_id = $(this).attr('id');
			
			var perem1 = $(this).parent().children('.peremenon').val();

			
						$.ajax({
                            type: "POST",
                            url: '/ajax/post_msg_zametki2.php',
                            data: "document_id="+document_id+"&pZametki2="+perem1,
                            success: function(msg){
						    swal("Отправлено!", "Ваше сообщение для управления успешно отправлено", "success");
                            }
                          });	
				  
});

	
//Сообщения на кнопок--------------------------------------------------------------------	
$('.zayavka_table').on('click', '.otzyv_zayavka', function () {
	
			zayavka_id = $(this).attr('id');	
	
            swal({
                        title: "Вы уверены?",
                        text: "Вы не сможете активировать отозванную заявку!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, отзываю!",
                        cancelButtonText: "Нет, не отзываю!",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
						 $.ajax({
                            type: "POST",
                            url: '/ajax/otzyv_zayavka_id.php',
                            data: "zayavka_id="+zayavka_id,
                            success: function(msg){
	                        $('.zayavka_table').load('/ajax/reload_list_zayavka.php',  function() {
                         	});
						  swal("Отозвана!", "Ваша заявка успешно отозвана", "success");
                            }
                          });	
                        } else {
                            swal("Отменено", "Ваша заявка цела и невредима :)", "error");
                        }
                    });
        });


$('.zayavka_table').on('click', '.delete_zayavka', function () {			
			
			zayavka_id = $(this).attr('id');
			
            swal({
                        title: "Вы уверены?",
                        text: "Вы не сможете восстановить удаленную заявку!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удаляю!",
                        cancelButtonText: "Нет, не удаляю!",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                            type: "POST",
                            url: '/ajax/delete_zayavka_id.php',
                            data: "zayavka_id="+zayavka_id,
                            success: function(msg){
	                        $('.zayavka_table').load('/ajax/reload_list_zayavka.php',  function() {
                         	});
                            swal("Удалена!", "Ваша заявка успешно удалена", "success");
                            }
                          });
                        } else {
                            swal("Отменено", "Ваша заявка цела и невредима :)", "error");
                        }
                    });
        });
		
		
		
		
		
		
$('.load_document_window').on('click', '.delete_file', function () {			
			
			file_id = $(this).attr('id');
			
            swal({
                        title: "Вы уверены?",
                        text: "Вы не сможете восстановить удаленный файл!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Да, удаляю!",
                        cancelButtonText: "Нет, не удаляю!",
                        closeOnConfirm: false,
                        closeOnCancel: false },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                            type: "POST",
                            url: '/ajax/file_delete.php',
                            data: "file_id="+file_id,
                            success: function(msg){
	                       // $('.zayavka_table').load('/ajax/reload_list_zayavka.php',  function() {
                         	//});
							
							  //$('.load_document_window').load('/ajax/list_documents.php', {zayavka_id:zayavka_id}, function() {
	               
	                        //  });
							
							
							   $('.load_document_window').load('/ajax/list_documents.php', {zayavka_id:zayavka_id}, function() {
	 
	                            });
							
							
							
                            swal("Удален!", "Ваш файл успешно удален", "success");
                            }
                          });
                        } else {
                            swal("Отменено", "Ваш файл остался цел и невредим :)", "error");
                        }
                    });
        });		
		
		
		
		
		

//--------------------------------------------------------------------------------	
	
$(".search_zayavka_temp").validate({
		submitHandler: function(form) {
			
			var m_data=$('.search_zayavka').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/search_zayavka.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.search_zayavka').slideUp(1000);
					$('#uspeh').html(result.message);
				}
				else
				{
					$('#uspeh').html(result.message);
				}
			}
			});

		},
       rules:{

         	klass_opasnosti:{
				required: true,
            },
			
			na_kakie_goda_zayavka:{
				required: true,
            },
			
			contact:{
				required: true,
            },			
			
			

       },
	   
       messages:{


			klass_opasnosti:{
                required:  "Выберите класс опасности",
            }, 

			na_kakie_goda_zayavka:{
                required:  "Укажите на какие года подается заявка",
            }, 

			contact:{
                required:  "Укажите контактное лицо и номер телефона",
            }, 			

			
   
       }
	   
		
    });	
	

		$(".register_zayavka").validate({
		submitHandler: function(form) {
			
			var m_data=$('.register_zayavka').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/register_zayavka.php',
			data: m_data,
			success: function(result){
				
				if(result.mailSent == true)
				{
					$('.register_zayavka').slideUp(1000);
					$('.uspeh').html(result.message);
				}
				
			    if(result.mailSent == false)
				{
					$('.uspeh').html(result.message);
				}
				
				if(result.rez == 1)
				{
					
					swal("Внимание", "Не найден заказчик", "error");
					$('#iin_bin_zak').focus();
					
					
				}	
	
			}
			
			
			
			
			
			});

		},
       rules:{

         	klass_opasnosti:{
				required: true,
            },
			
			na_kakie_goda_zayavka:{
				required: true,
            },
			
			contact:{
				required: true,
            },			

       },
	   
       messages:{

			klass_opasnosti:{
                required:  "Выберите класс опасности",
            }, 

			na_kakie_goda_zayavka:{
                required:  "Укажите на какие года подается заявка",
            }, 

			contact:{
                required:  "Укажите контактное лицо и номер телефона",
            }, 			

       }
	   
		
    });

	
	$(".register_zakazchik_from_razrabotchik").validate({
		submitHandler: function(form) {
			
			var m_data=$('.register_zakazchik_from_razrabotchik').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/register_zakazchik_from_razrabotchik.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
				//document.location.href = "/";
				window.location = "/?page=register_zayavka"
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin_bin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},
			
         	oblastmz:{
				required: true,
            },
			
			rayon:{
				required: true,
            },
			
			opf:{
				required: true,
            },			
			
			vid_deyatelmznosti:{
				required: true,
            },	
			
			nazvanie_predpriyatiya_rus:{
				required: true,
            },				
			
			nazvanie_predpriyatiya_kaz:{
				required: true,
            },	

			fio_rukovoditelya:{
				required: true,
            },
			
			naselennyy_punkt:{
				required: true,
            },		
			
			adres:{
				required: true,
            },
			
			telefon:{
				required: true,
            },
		
		
			email:{
				required: true,
				email:true,
            },
			
			email1:{
				required: true,
				email:true,
				equalTo: "#email",
				
            },
			
			pswrd:{
				required: true,
            },
			

			pswrd1: {
			required: true,
            equalTo: "#pswrd",
             }

       },
	   
       messages:{
		   
			iin_bin:{
                required:  "Введите БИН/ИИН организации",
                digits:    "Для ввода допускаются только цифры",				
                minlength: "Длина БИН/ИИН не должна быть меньше 12 символов",
                maxlength: "Длина БИН/ИИН не должна быть больше 12 символов",
            }, 

			oblastmz:{
                required:  "Выберите область",
            }, 

			rayon:{
                required:  "Выберите район",
            }, 

			opf:{
                required:  "Выберите ОПФ",
            }, 			

			vid_deyatelmznosti:{
                required:  "Выберите вид деятельности",
            }, 			

			nazvanie_predpriyatiya_rus:{
                required:  "Введите название организации на русском языке",
            }, 				
			
			nazvanie_predpriyatiya_kaz:{
                required:  "Введите название организации на казахском языке",
            }, 			
			
			fio_rukovoditelya:{
                required:  "Введите ФИО руководителя",
            }, 				
			
			naselennyy_punkt:{
                required:  "Введите название населенного пункта",
            }, 	

			adres:{
                required:  "Введите адрес",
            }, 	

			telefon:{
                required:  "Введите телефон",
            }, 	

			email:{
                required:  "Введите электронную почту",
				email:     "Введите корректный E-mail",
            }, 	

			email1:{
                required:  "Введите для проверки электронную почту",
				equalTo:   "Электронная почта не совпадает",
				email:     "Введите корректный E-mail",
            }, 		

			pswrd:{
                required:  "Введите пароль",
            }, 	

			pswrd1:{
                required:  "Введите пароль для проверки",
				equalTo:   "Пароли не совпадают",
            }, 
   
       }
	   
		
    });
	
	
	
	
	$(".register_zakazchik").validate({
		submitHandler: function(form) {
			
			var m_data=$('.register_zakazchik').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/register_zakazchik.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
				//document.location.href = "/";
				window.location = "/"
				}
				else
				{
					$('#neuspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin_bin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},
			
         	oblastmz:{
				required: true,
            },
			
			rayon:{
				required: true,
            },
			
			opf:{
				required: true,
            },			
			
			vid_deyatelmznosti:{
				required: true,
            },	
			
			nazvanie_predpriyatiya_rus:{
				required: true,
            },				
			
			nazvanie_predpriyatiya_kaz:{
				required: true,
            },	

			fio_rukovoditelya:{
				required: true,
            },
			
			naselennyy_punkt:{
				required: true,
            },		
			
			adres:{
				required: true,
            },
			
			telefon:{
				required: true,
            },
		
		
			email:{
				required: true,
				email:true,
            },
			
			email1:{
				required: true,
				email:true,
				equalTo: "#email",
				
            },
			
			pswrd:{
				required: true,
            },
			

			pswrd1: {
			required: true,
            equalTo: "#pswrd",
             }

       },
	   
       messages:{
		   
			iin_bin:{
                required:  "Введите БИН/ИИН организации",
                digits:    "Для ввода допускаются только цифры",				
                minlength: "Длина БИН/ИИН не должна быть меньше 12 символов",
                maxlength: "Длина БИН/ИИН не должна быть больше 12 символов",
            }, 

			oblastmz:{
                required:  "Выберите область",
            }, 

			rayon:{
                required:  "Выберите район",
            }, 

			opf:{
                required:  "Выберите ОПФ",
            }, 			

			vid_deyatelmznosti:{
                required:  "Выберите вид деятельности",
            }, 			

			nazvanie_predpriyatiya_rus:{
                required:  "Введите название организации на русском языке",
            }, 				
			
			nazvanie_predpriyatiya_kaz:{
                required:  "Введите название организации на казахском языке",
            }, 			
			
			fio_rukovoditelya:{
                required:  "Введите ФИО руководителя",
            }, 				
			
			naselennyy_punkt:{
                required:  "Введите название населенного пункта",
            }, 	

			adres:{
                required:  "Введите адрес",
            }, 	

			telefon:{
                required:  "Введите телефон",
            }, 	

			email:{
                required:  "Введите электронную почту",
				email:     "Введите корректный E-mail",
            }, 	

			email1:{
                required:  "Введите для проверки электронную почту",
				equalTo:   "Электронная почта не совпадает",
				email:     "Введите корректный E-mail",
            }, 		

			pswrd:{
                required:  "Введите пароль",
            }, 	

			pswrd1:{
                required:  "Введите пароль для проверки",
				equalTo:   "Пароли не совпадают",
            }, 
   
       }
	   
		
    });
	
	$(".update_zakazchik_from_razrabotchik").validate({
		submitHandler: function(form) {
			
			var m_data=$('.update_zakazchik_from_razrabotchik').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/update_zakazchik_from_razrabotchik.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
				    $('.update_zakazchik_from_razrabotchik').slideUp(1000);
					$('.uspeh').html(result.message);
				// swal("Профиль обновлен!", "Профиль заказчика обновлён успешно", "success");
				
				
				
				}
				else
				{
					$('.uspeh').html(result.message);
				//swal("Профиль не обновлен!", "Введены не все данные", "success");
				}
			}
			});

		},
       rules:{
		   
			iin_bin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},
			
         	oblastmz:{
				required: true,
            },
			
			rayon:{
				required: true,
            },
			
			opf:{
				required: true,
            },			
			
			vid_deyatelmznosti:{
				required: true,
            },	
			
			nazvanie_predpriyatiya_rus:{
				required: true,
            },				
			
			nazvanie_predpriyatiya_kaz:{
				required: true,
            },	

			fio_rukovoditelya:{
				required: true,
            },
			
			naselennyy_punkt:{
				required: true,
            },		
			
			adres:{
				required: true,
            },
			
			telefon:{
				required: true,
            },
		
		
		

       },
	   
       messages:{
		   
			iin_bin:{
                required:  "Введите БИН/ИИН организации",
                digits:    "Для ввода допускаются только цифры",				
                minlength: "Длина БИН/ИИН не должна быть меньше 12 символов",
                maxlength: "Длина БИН/ИИН не должна быть больше 12 символов",
            }, 

			oblastmz:{
                required:  "Выберите область",
            }, 

			rayon:{
                required:  "Выберите район",
            }, 

			opf:{
                required:  "Выберите ОПФ",
            }, 			

			vid_deyatelmznosti:{
                required:  "Выберите вид деятельности",
            }, 			

			nazvanie_predpriyatiya_rus:{
                required:  "Введите название организации на русском языке",
            }, 				
			
			nazvanie_predpriyatiya_kaz:{
                required:  "Введите название организации на казахском языке",
            }, 			
			
			fio_rukovoditelya:{
                required:  "Введите ФИО руководителя",
            }, 				
			
			naselennyy_punkt:{
                required:  "Введите название населенного пункта",
            }, 	

			adres:{
                required:  "Введите адрес",
            }, 	

			telefon:{
                required:  "Введите телефон",
            }, 	

		
   
       }
	   
		
    });
	

	$(".update_zakazchik").validate({
		submitHandler: function(form) {
			
			var m_data=$('.update_zakazchik').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/update_zakazchik.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
				    $('.update_zakazchik').slideUp(1000);
					$('.uspeh').html(result.message);
				}
				else
				{
					$('.uspeh').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin_bin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},
			
         	oblastmz:{
				required: true,
            },
			
			rayon:{
				required: true,
            },
			
			opf:{
				required: true,
            },			
			
			vid_deyatelmznosti:{
				required: true,
            },	
			
			nazvanie_predpriyatiya_rus:{
				required: true,
            },				
			
			nazvanie_predpriyatiya_kaz:{
				required: true,
            },	

			fio_rukovoditelya:{
				required: true,
            },
			
			naselennyy_punkt:{
				required: true,
            },		
			
			adres:{
				required: true,
            },
			
			telefon:{
				required: true,
            },
		
		
			email11:{
				required: true,
				email:true,
            },
			
			email22:{
				required: true,
				email:true,
				equalTo: "#email11",
				
            },
			
			pswrd11:{
				required: true,
            },
			

			pswrd22: {
			required: true,
            equalTo: "#pswrd11",
             }

       },
	   
       messages:{
		   
			iin_bin:{
                required:  "Введите БИН/ИИН организации",
                digits:    "Для ввода допускаются только цифры",				
                minlength: "Длина БИН/ИИН не должна быть меньше 12 символов",
                maxlength: "Длина БИН/ИИН не должна быть больше 12 символов",
            }, 

			oblastmz:{
                required:  "Выберите область",
            }, 

			rayon:{
                required:  "Выберите район",
            }, 

			opf:{
                required:  "Выберите ОПФ",
            }, 			

			vid_deyatelmznosti:{
                required:  "Выберите вид деятельности",
            }, 			

			nazvanie_predpriyatiya_rus:{
                required:  "Введите название организации на русском языке",
            }, 				
			
			nazvanie_predpriyatiya_kaz:{
                required:  "Введите название организации на казахском языке",
            }, 			
			
			fio_rukovoditelya:{
                required:  "Введите ФИО руководителя",
            }, 				
			
			naselennyy_punkt:{
                required:  "Введите название населенного пункта",
            }, 	

			adres:{
                required:  "Введите адрес",
            }, 	

			telefon:{
                required:  "Введите телефон",
            }, 	

			email11:{
                required:  "Введите электронную почту",
				email:     "Введите корректный E-mail",
            }, 	

			email22:{
                required:  "Введите для проверки электронную почту",
				equalTo:   "Электронная почта не совпадает",
				email:     "Введите корректный E-mail",
            }, 		

			pswrd11:{
                required:  "Введите пароль",
            }, 	

			pswrd22:{
                required:  "Введите пароль для проверки",
				equalTo:   "Пароли не совпадают",
            }, 
   
       }
	   
		
    });

	$(".login_zakazchik").validate({
		submitHandler: function(form) {
			
			var m_data=$('.login_zakazchik').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/login_zakazchik.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
				window.location = "/";
				}
				else
				{
					$('.result').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin_bin:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},

			pswrd:{
				required: true,
            },


       },
	   
       messages:{
		   
			iin_bin:{
                required:  "Введите БИН/ИИН организации",
                digits:    "Для ввода допускаются только цифры",				
                minlength: "Длина БИН/ИИН не должна быть меньше 12 символов",
                maxlength: "Длина БИН/ИИН не должна быть больше 12 символов",
            }, 

			pswrd:{
                required:  "Введите пароль",
            }, 	

   
       }
	   
		
    });	
	
	$(".login_razrabotchik").validate({
		submitHandler: function(form) {
			
			var m_data=$('.login_razrabotchik').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/login_razrabotchik.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
				window.location = "/";
				}
				else
				{
					$('.result1').html(result.message);
				}
			}
			});

		},
       rules:{
		   
			iin_bin1:{
				required: true,
				digits:true,
				minlength: 12,
                maxlength: 12,	
			},

			pswrd1:{
				required: true,
            },


       },
	   
       messages:{
		   
			iin_bin1:{
                required:  "Введите БИН/ИИН организации",
                digits:    "Для ввода допускаются только цифры",				
                minlength: "Длина БИН/ИИН не должна быть меньше 12 символов",
                maxlength: "Длина БИН/ИИН не должна быть больше 12 символов",
            }, 

			pswrd1:{
                required:  "Введите пароль",
            }, 	

   
       }
	   
		
    });		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('.resultFilter').on('click', '.pagess', function(e){
		
		e.preventDefault();
		
		var numbers = $(this).attr('numbers');
		var material = $('.material').val();
		var kategoriya_lota_id = $('.kategoriya_lota_id').val();
		var status_lota_id = $('.status_lota_id:checked').val();
		var regiony_id = $('.regiony_id').val();
		
		$(".resultFilter").load(
		  "/ajax/filtergrid.php",
		  {
			material: material,
			kategoriya_lota_id: kategoriya_lota_id,
			status_lota_id: status_lota_id,
			regiony_id: regiony_id,
			numbers: numbers
		  });	
		
	});
	
	$('.filter_postav').on('click', '.win', function(e){
		
		e.preventDefault();
		
		var otvet_na_zayavku_id = $(this).attr('otvet_na_zayavku_id');
		
		var kommentarii_zak = prompt('Ваш комментарий?', '');
		
		if(kommentarii_zak != null)
		{
			$.post(
				  "/ajax/otvet_na_zayavku.php",
				  {
					otvet_na_zayavku_id: otvet_na_zayavku_id,
					kommentarii_zak: kommentarii_zak
				  },
				  function(result)
				  {
					 alert(result.message);
					 
					 $('.win').hide();
				  }
				);
		}
	});
	
	
	$(".select_record").click(function(e){
		
		e.preventDefault();
		
		var zayavka_id = $(this).parent().attr('select_record');
		
		$(".select_record").parent().removeClass('activeTable');
		
		$(this).parent().addClass('activeTable');
		
		
		$(".filter_postav").load("/ajax/uchastniki_lota.php",{
						zayavka_id: zayavka_id});
						
						
						
		
			$('html, body').animate({
				scrollTop: $("#lot_history").offset().top
			}, 500);
		
		
		
	});
	
	
	$('.resultFilter').on('click', '.izbrannoe', function(e){
		
		e.preventDefault();
		
		knopka = $(this);
		
		var ident = knopka.parent().parent().children('.ident').val();
		
		var yes = confirm('Добавить лот в избранное?');
		
		if(yes)
		{
			$.post(
			  "/ajax/v_izbrannoe.php",
			  {
				ident: ident
			  },
			  onAjaxSuccess
			);
			 
			
		}
		
	});
	
	function onAjaxSuccess(data)
			{
				//alert(data.message);
				knopka.parent().html(data.knop);
			}
			
			
		$('.resultFilter').on('click', '.notizbrannoe', function(e){
		
		e.preventDefault();
		
		knopka = $(this);
		
		var ident = knopka.parent().parent().children('.ident').val();
		
		var yes = confirm('Убрать лот из избранного?');
		
		if(yes)
		{
			$.post(
			  "/ajax/iz_izbrannogo.php",
			  {
				ident: ident
			  },
			  onAjaxNotSuccess
			);
			 
			
		}
		
	});
	
	function onAjaxNotSuccess(data)
			{
				//alert(data.message);
				knopka.parent().html(data.knop);
			}		

	
	$('.contry').change(function(){
		
		var contry = $(this).val();
		
		$(".region").load(
		  "/ajax/filterregion.php",
		  {
			contry: contry
		  });
		
	});
	

	
	$(".formfeatbak").validate({
		submitHandler: function(form) {
			
			var m_data=$('.formfeatbak').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/contaktform.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					$('.formfeatbak').slideUp(1000);
					$('.uspeh').html(result.message);
				}
				else
				{
					$('.neuspeh').html(result.message);
				}
				
			}
			});
			
			
		},
       rules:{
			name:{
				required: true,
				
			},
            email:{
				required: true,
                email: true,
            },
			
			phone:{
				required: true,
				digits:true,

			},

			message:{
				required: true,
                minlength: 6,
                maxlength: 500,
				
            }
       },
       messages:{

			name:{
                required: "Введите ваше имя"
            },
	   
            email:{
                email: "Введите корректный E-mail",
				required:  "Введите ваш E-mail" 
            },
			
			 phone:{
                digits: "Поле телефон является цифровым полем",
				required:  "Введите ваш телефон",

            },

			message:{
                required:  "Введите сообщение",
                minlength: "Длина сообщения не должна быть меньше 6 символов",
                maxlength: "Длина сообщения не должна быть больше 500 символов",
				
            }
        }
	 		
    });
	
	
	$(".insertquestion").validate({
		submitHandler: function(form) {
			
			var m_data=$('.insertquestion').serialize();
			$.ajax({
			type: 'POST',
			url: '/ajax/insertquestion.php',
			data: m_data,
			success: function(result){
				if(result.mailSent == true)
				{
					
					$('.svernut').slideUp(1000);;
					$('.svernut1').hide();
					$('#uspeh').html(result.message);
				}
				else
				{
					$('#uspeh').html(result.message);
				}
				
			}
			});
			
			
		},
       rules:{
			
			imya:{
				required: true
				
			},
			
			
			
			email:{
				required: true,
				email:true,
				
			},	


	
			mob:{
				required: true
				
			},
			
			tip_voprosa:{
				required: true
				
			},			
			
			
			
			vopros:{
				required: true
				
			},				
			
			
			

			
			

       },
       messages:{

			imya:{
                required: "Введите как к Вам обращаться"
            },
			
			
			email:{
                required: "Введите вашу электронную почту",
				email:    "Введите корректный E-mail",
            },
			
			mob:{
                required: "Введите ваш номер телефона"
            },	


			tip_voprosa:{
                required: "Выберите тип вопроса"
            },	


			vopros:{
                required: "Введите ваш вопрос"
            },	

	
        }
	 		
    });
	
	

});
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Text;

namespace Student
{
    public partial class WebForm2 : System.Web.UI.Page
    {

        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader dr;

        protected void LoadSuburb1()
        {

            if (!this.IsPostBack)
            {
                using (SqlConnection conn = new SqlConnection(connstr))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT id, suburb FROM suburb order by id"))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Connection = conn;
                        conn.Open();
                        DropDownSuburb.DataSource = cmd.ExecuteReader();
                        DropDownSuburb.DataTextField = "suburb";
                        DropDownSuburb.DataValueField = "id";
                        DropDownSuburb.DataBind();
                        conn.Close();
                    }
                }
             //   DropDownSuburb.Items.Insert(0, new ListItem("--Select suburb--", "0"));
            }

        }

        protected void LoadDropdownList(string pTableName,string pFieldName, System.Web.UI.WebControls.DropDownList pControl)
        {
            SqlConnection conn = new SqlConnection(connstr);
            conn.Open();
            string query = "select id,"+ pFieldName + " from "+ pTableName + " order by id desc";
            SqlCommand cmd = new SqlCommand(query, conn);
            dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    pControl.Items.Insert(0, new ListItem(dr[pFieldName].ToString(), dr["id"].ToString()));
                }
            }

        }




        protected void ClearAll()
        {
            LoadDropdownList("suburb", "suburb", DropDownSuburb);
            LoadDropdownList("gender", "gender", DropDownGender);
            LoadDropdownList("role", "role", DropDownRole);
            LabelWarningName.Visible = false;
            LabelWarningSurname.Visible = false;
            LabelWarningEmail.Visible = false;
            LabelWarningPassword.Visible = false;
        }

        protected void Page_init(object sender, EventArgs e)
        {
            ClearAll();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
          
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string dt = Request.Form[txtDate_of_birth1.UniqueID];
        }

        protected void ButtonRegister_Click(object sender, EventArgs e)
        {


            if (TextBoxName.Text == "")
            {
                LabelWarningName.Visible = true;
                TextBoxName.Focus();
                return;

            }

            LabelWarningName.Visible = false;

            if (TextBoxSurname.Text == "")
            {
                LabelWarningSurname.Visible = true;
                TextBoxSurname.Focus();
                return;

            }

            LabelWarningSurname.Visible = false;


            conn = new SqlConnection(connstr);
            SqlCommand cmd1 = new SqlCommand("SELECT * from student where name = '" + TextBoxName.Text + "' and surname = '" + TextBoxSurname.Text + "'", conn);
            SqlDataAdapter adp1 = new SqlDataAdapter(cmd1);
            DataTable dt1 = new DataTable();
            adp1.Fill(dt1);
            if (dt1.Rows.Count > 0)
            {
                //We have such a user in our table
                LabelWarningSurname.Visible = true;
                LabelWarningSurname.Text = "We have already user with the same fullname in our database";
                TextBoxName.Focus();
                return;
            }









            if (TextBoxEmail.Text == "")
            {
                LabelWarningEmail.Visible = true;
                LabelWarningEmail.Text = "Field [Email] can not be empty";
                TextBoxEmail.Focus();
                return;

            }

            Regex emailRegex = new Regex(@"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");




            if (emailRegex.IsMatch(TextBoxEmail.Text)==false)
            {
                //  MessageBox.Show(TextBoxEmail.Text + "matches the expected format.", "Attention");
                LabelWarningEmail.Visible = true;
                LabelWarningEmail.Text = "Email field is in a wrong format";
                TextBoxEmail.Focus();
                return;
            }

            LabelWarningEmail.Visible = false;




            if (TextBoxPassword.Text == "")
            {
                LabelWarningPassword.Visible = true;
                LabelWarningPassword.Text = "Field [Password] can not be empty";
                TextBoxPassword.Focus();
                return;
            }


            if (TextBoxRepeatPassword.Text == "")
            {
                LabelWarningPassword.Visible = true;
                LabelWarningPassword.Text = "Field [Repeat Password] can not be empty";
                TextBoxRepeatPassword.Focus();
                return;
            }


            if (TextBoxPassword.Text != TextBoxRepeatPassword.Text)
            {
                LabelWarningPassword.Visible = true;
                LabelWarningPassword.Text = "Field [Repeat Password] must be similar";
                TextBoxRepeatPassword.Focus();
                return;
            }




            conn = new SqlConnection(connstr);
            SqlCommand cmd = new SqlCommand("SELECT * from student where email = '" + TextBoxEmail.Text+"'", conn);
            SqlDataAdapter adp = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            adp.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                //We have such a user in our table
                LabelWarningEmail.Visible = true;
                LabelWarningEmail.Text = "We have already user with this email in our database";
                TextBoxEmail.Focus();
                return;
            }
            //else
            //{
            //   // LabelBadPassword.Visible = true;
            //    //No such a user in our table
            //}







            LabelWarningPassword.Visible = false;
            var pPzzz = "111";
            // string[] dueDateSplit = Request.Form[txtDate_of_birth1.UniqueID].Split('/');
            // DateTime due = new DateTime(dueDateSplit[2], dueDateSplit[1], dueDateSplit[0]);

            string dta = Convert.ToString(Request.Form[txtDate_of_birth1.UniqueID]);
            DateTime pDate = DateTime.ParseExact(dta, "dd/MM/yyyy", null);

            conn = new SqlConnection(connstr);
            cmd = new SqlCommand("Insert into student (name,surname,email,mobile_phone,suburb_id,street,house,gender_id,date_of_birth,password,role_id) values (@name,@surname,@email,@mobile_phone,@suburb_id,@street,@house,@gender_id,@date_of_birth,@password,@role_id)", conn);
            cmd.Parameters.AddWithValue("@name", TextBoxName.Text);
            cmd.Parameters.AddWithValue("@surname", TextBoxSurname.Text);
            cmd.Parameters.AddWithValue("@email", TextBoxEmail.Text);
            cmd.Parameters.AddWithValue("@mobile_phone", TextBoxMobile_phone.Text);
            cmd.Parameters.AddWithValue("@suburb_id", DropDownSuburb.SelectedValue);
            cmd.Parameters.AddWithValue("@street", TextBoxStreet.Text);
            cmd.Parameters.AddWithValue("@house", TextBoxHouse.Text);
            cmd.Parameters.AddWithValue("@password", TextBoxPassword.Text);
            cmd.Parameters.AddWithValue("@role_id", DropDownRole.SelectedValue);
            cmd.Parameters.AddWithValue("@gender_id", DropDownGender.SelectedValue);
            cmd.Parameters.AddWithValue("@date_of_birth", pDate);
   
                conn.Open();
            if (cmd.ExecuteNonQuery() == 1)
            {
                labelMessage.Text = "Record added sucsessfully";


                //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                //mail.To.Add(TextBoxEmail.Text);
                //mail.From = new MailAddress("abramov.developer@gmail.com", "Email head", System.Text.Encoding.UTF8);
                //mail.Subject = "Thanks for registration";
                //mail.SubjectEncoding = System.Text.Encoding.UTF8;
                //mail.Body = "This is Email Body Text";
                //mail.BodyEncoding = System.Text.Encoding.UTF8;
                //mail.IsBodyHtml = true;
                //mail.Priority = MailPriority.High;
                //SmtpClient client = new SmtpClient();
                //client.Credentials = new System.Net.NetworkCredential("abramov.developer@gmail.com", pPzzz);
                //client.Port = 587;
                //client.Host = "smtp.gmail.com";
                //client.EnableSsl = true;
                //try
                //{
                //    client.Send(mail);
                //}
                //catch (Exception ex)
                //{
                //    Exception ex2 = ex;
                //    string errorMessage = string.Empty;
                //    while (ex2 != null)
                //    {
                //        errorMessage += ex2.ToString();
                //        ex2 = ex2.InnerException;
                //    }
                //}












                Response.Redirect("Index.aspx");
            }

            if (cmd.ExecuteNonQuery() == 1)
            {
                labelMessage.Text = "We have a problem";

            }

            conn.Close();

        }

        protected void ButtonHome_Click(object sender, EventArgs e)
        {
            Response.Redirect("Index.aspx");
        }

       
    }
}
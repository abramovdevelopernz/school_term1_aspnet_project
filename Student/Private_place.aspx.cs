﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text.RegularExpressions;

namespace Student
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        private string connstr = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["MyConnection"].ConnectionString;
        private SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader dr;


        protected void LoadDropdownList(string pTableName, string pFieldName, System.Web.UI.WebControls.DropDownList pControl)
        {
            SqlConnection conn = new SqlConnection(connstr);
            conn.Open();
            string query = "select id," + pFieldName + " from " + pTableName + " order by id desc";
            SqlCommand cmd = new SqlCommand(query, conn);
            dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    pControl.Items.Insert(0, new ListItem(dr[pFieldName].ToString(), dr["id"].ToString()));
                }
            }

        }




        protected void ClearAll()
        {
            LoadDropdownList("suburb", "suburb", DropDownSuburb);
            LoadDropdownList("course", "course", DropDownCourse);
            LoadDropdownList("gender", "gender", DropDownGender);
            LabelWarningName.Visible = false;
            LabelWarningSurname.Visible = false;
            labelMessage1.Visible = false;


        }




        protected void GetCurrentStudent()
        {
        conn = new SqlConnection(connstr);
        SqlCommand cmd = new SqlCommand("SELECT * from student where id = " + Session["current_id"].ToString(), conn);
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);

         
            if (dt.Rows.Count > 0)
            {
                TextBoxName.Text = dt.Rows[0]["name"].ToString();
                TextBoxSurname.Text = dt.Rows[0]["surname"].ToString();
                TextBoxMobile_phone.Text = dt.Rows[0]["mobile_phone"].ToString();
                DropDownSuburb.Text = dt.Rows[0]["suburb_id"].ToString();
                TextBoxStreet.Text = dt.Rows[0]["street"].ToString();
                TextBoxHouse.Text = dt.Rows[0]["house"].ToString();
                DropDownCourse.Text = dt.Rows[0]["course_id"].ToString();
                txtDate_of_birth2.Text = Convert.ToDateTime(dt.Rows[0]["date_of_birth"].ToString()).ToString("dd/MM/yyyy");
                DropDownGender.Text = dt.Rows[0]["gender_id"].ToString();
            }

        }


        protected void LoadGridViewStudent()
        {
            using (SqlConnection sqlCon = new SqlConnection(connstr))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter(
                    "Select " +
                    "s.id as student_id," +
                    "s.name," +
                    "s.surname," +
                    "s.email," +
                    "s.mobile_phone," +
                    "suburb.suburb," +
                    "s.street," +
                    "s.house," +
                    "course.course," +
                    "s.date_of_birth," +
                    "s.password," +
                    "gender.gender," +
                    "s.approved " +
                    "from " +
                    "student s, course, gender, suburb " +
                    "where " +
                    "s.course_id=course.id and " +
                    "s.gender_id=gender.id and " +
                    "s.suburb_id=suburb.id and " +
                    "role_id=2 and " +
                    "deleted=0"
                    , sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridViewStudent.DataSource = dtbl;
                GridViewStudent.DataBind();
            }
        }


        protected void lnkSelect_Click(object sender, EventArgs e)
        {
            int pStudent_id = Convert.ToInt32((sender as LinkButton).CommandArgument);
            //MessageBox.Show(pStudent_id.ToString());


            DialogResult result = MessageBox.Show("Do you want to delete current student with ID "+ pStudent_id.ToString() + "?", "Confirmation", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {

                conn = new SqlConnection(connstr);
                cmd = new SqlCommand("Delete from student where id = " + pStudent_id, conn);
                conn.Open();

                if (cmd.ExecuteNonQuery() == 1)
                {
                    
                    LoadGridViewStudent();
                    MessageBox.Show("Record successfully deleted");


                }

               // if (cmd.ExecuteNonQuery() == 0)
               // {
              //      MessageBox.Show("We have a problem");

               // }
                conn.Close();
            }
            else if (result == DialogResult.No)
            {
                //MessageBox.Show("Not Delete");
            };


        }



        protected void GetCurrentCourse_lesson()
        {
            conn = new SqlConnection(connstr);
            cmd = new SqlCommand(
                "Select * from course_lesson where course_id="+ DropDownCourse.SelectedValue
                , conn);
            System.Data.DataTable dt = new System.Data.DataTable();
            conn.Open();

            dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                dt.Load(dr);
                GridView2.DataSource = dt;
                GridView2.DataBind();
            }
            {
                dt.Load(dr);
                GridView2.DataSource = dt;
                GridView2.EmptyDataText = "No records to show";
                GridView2.DataBind();
            }


        }







        protected void GetAllStudent()
        {
            conn = new SqlConnection(connstr);
            cmd = new SqlCommand(
                "Select "+
                "s.id as student_id," +
                "s.name," +
                "s.surname," +
                "s.email," +
                "s.mobile_phone," +
                "suburb.suburb," +
                "s.street," +
                "s.house," +
                "course.course," +
                "s.date_of_birth," +
                "s.password," +
                "gender.gender," +
                "s.approved " +
                "from "+
                "student s, course, gender, suburb "+
                "where "+
                "s.course_id=course.id and "+
                "s.gender_id=gender.id and "+
                "s.suburb_id=suburb.id and "+
                "role_id=2 and "+
                "deleted=0"
                , conn);
            System.Data.DataTable dt = new System.Data.DataTable();
            conn.Open();

            dr = cmd.ExecuteReader();

            if (dr.HasRows)
            {
                dt.Load(dr);
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            {
                dt.Load(dr);
                GridView1.DataSource = dt;
                GridView1.EmptyDataText = "No students to show";
                GridView1.DataBind();
            }


        }

        protected void Page_init(object sender, EventArgs e)
        {
            ClearAll();

            if (Session["current_id"] != null)
            {
                LabelMessage.Text = "Welcome to your private area " + Session["current_full_name"];




                if (Session["current_role_id"].ToString() == "2")
                {
                    GetCurrentStudent();
                    PanelStudent.Visible = true;
                    PanelAdministrator.Visible = false;
                }

                if (Session["current_role_id"].ToString() == "3")
                {
                    //GetAllStudent();
                    LoadGridViewStudent();
                    //GetAllStudent();
                    PanelStudent.Visible = false;
                    PanelAdministrator.Visible = true;
                }


            }
            else
            {
                LabelMessage.Text = "Attention!  This is the private area. You're not authorized person";
            }




        }

        protected void Page_Load(object sender, EventArgs e)
        {

           
        }

        protected void ButtonLog_out_Click(object sender, EventArgs e)
        {
            Session.Remove("current_id");
            Session.Remove("current_role_id");
            Session.Remove("current_full_name");
            Response.Redirect("Index.aspx");
        }

        protected void ButtonUpdate_Click(object sender, EventArgs e)
        {

            if (TextBoxName.Text.Trim() == "")
            {
                MessageBox.Show("Field [Name] can not be empty");
                TextBoxName.Focus();
                return;
            }

            if (TextBoxSurname.Text.Trim() == "")
            {
                MessageBox.Show("Field [Surname] can not be empty");
                TextBoxSurname.Focus();
                return;
            }


            if (TextBoxMobile_phone.Text.Trim() == "")
            {
                MessageBox.Show("Field [MobilePhone] can not be empty");
                TextBoxMobile_phone.Focus();
                return;

            }

            if (TextBoxStreet.Text.Trim() == "")
            {
                MessageBox.Show("Field [Street] can not be empty");
                TextBoxStreet.Focus();
                return;

            }

            if (TextBoxHouse.Text.Trim() == "")
            {
                MessageBox.Show("Field [House] can not be empty");
                TextBoxHouse.Focus();
                return;

            }


            string dt = Convert.ToString(Request.Form[txtDate_of_birth2.UniqueID]);
            DateTime pDate = DateTime.ParseExact(dt, "dd/MM/yyyy", null);

            conn = new SqlConnection(connstr);
            cmd = new SqlCommand("Update student set name=@name, surname=@surname, mobile_phone=@mobile_phone, suburb_id=@suburb_id, street=@street, house=@house, course_id=@course_id, gender_id=@gender_id, date_of_birth=@date_of_birth where id = " + Session["current_id"].ToString(), conn);
            cmd.Parameters.AddWithValue("@name", TextBoxName.Text);
            cmd.Parameters.AddWithValue("@surname", TextBoxSurname.Text);
            cmd.Parameters.AddWithValue("@mobile_phone", TextBoxMobile_phone.Text);
            cmd.Parameters.AddWithValue("@suburb_id", DropDownSuburb.SelectedValue);
            cmd.Parameters.AddWithValue("@street", TextBoxStreet.Text);
            cmd.Parameters.AddWithValue("@house", TextBoxHouse.Text);
            cmd.Parameters.AddWithValue("@course_id", DropDownCourse.SelectedValue);
            cmd.Parameters.AddWithValue("@gender_id", DropDownGender.SelectedValue);
            cmd.Parameters.AddWithValue("@date_of_birth", pDate);
            conn.Open();

          //  MessageBox.Show(cmd.CommandText);

            labelMessage1.Visible = true;

            if (cmd.ExecuteNonQuery() == 1)
            {
                labelMessage1.Text = "Record updated sucsessfully";

            }

            if (cmd.ExecuteNonQuery() == 0)
            {
                labelMessage1.Text = "We have a problem";

            }

            conn.Close();

        }

        protected void DropDownCourse_TextChanged(object sender, EventArgs e)
        {
            GetCurrentCourse_lesson();
        }

        protected void ButtonPasswordChange_Click(object sender, EventArgs e)
        {
            //System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            //mail.To.Add(TextBoxEmail.Text);
            //mail.From = new MailAddress("abramov.developer@gmail.com", "Email head", System.Text.Encoding.UTF8);
            //mail.Subject = "Password change request notification";
            //mail.SubjectEncoding = System.Text.Encoding.UTF8;
            //mail.Body = "This is Email Body Text";
            //mail.BodyEncoding = System.Text.Encoding.UTF8;
            //mail.IsBodyHtml = true;
            //mail.Priority = MailPriority.High;
            //SmtpClient client = new SmtpClient();
            //client.Credentials = new System.Net.NetworkCredential("abramov.developer@gmail.com", pPzzz);
            //client.Port = 587;
            //client.Host = "smtp.gmail.com";
            //client.EnableSsl = true;
            //try
            //{
            //    client.Send(mail);
            //}
            //catch (Exception ex)
            //{
            //    Exception ex2 = ex;
            //    string errorMessage = string.Empty;
            //    while (ex2 != null)
            //    {
            //        errorMessage += ex2.ToString();
            //        ex2 = ex2.InnerException;
            //    }
            //}
        }
    }
}
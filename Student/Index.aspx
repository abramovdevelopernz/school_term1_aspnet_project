﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Student.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">



    <div class="login-container">
      <div class="row">
         <div class="col-md-12">
            <div class="text-center m-b-md">
               <h3> <strong>Student database </strong></h3>
            </div>

            <div class="hpanel">
               <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#tab-1">SignIn</a></li>
                  <li class=""><a data-toggle="tab" href="#tab-2">Help</a></li> 
               </ul>
               <div class="tab-content">
                  <div id="tab-1" class="tab-pane active">
                     <div class="panel-body">
                      
                           <form>
                              <div class="form-group">
                                 <label class="control-label" for="username" >Email</label>
                                  <asp:TextBox ID="TextBoxEmail" class="form-control" placeholder="Your email"  runat="server"></asp:TextBox>
                                  <asp:Label class="text-danger" ID="LabelWarningEmail" runat="server"   Text="Field [Email] can not be empty"></asp:Label>
                              </div>
                              <div class="form-group">
                               <label class="control-label" for="password">Password</label>
                               <asp:TextBox ID="TextBoxPassword" class="form-control" placeholder="Your password"  runat="server" ></asp:TextBox>
                               <asp:Label class="text-danger" ID="LabelWarningPassword" runat="server" Text="Field [Password] can not be empty"></asp:Label>     
                              </div>
                               <asp:Label class="text-danger" ID="LabelBadPassword" runat="server" Text="Email or password is invalid"></asp:Label>
                               <asp:Button  ID="ButtonSignIn" class="btn btn-success  btn-block" runat="server" OnClick="ButtonSignIn_Click" Text="Sign In" />
                               <asp:Button ID="ButtonRegister" class="btn btn-default  btn-block" runat="server" Text="Register" OnClick="ButtonRegister_Click" />
                           </form>
                     </div>
                  </div>
           
				  <div id="tab-2" class="tab-pane">
                     <div class="panel-body">
					 1. Help 1.
					 <br>
					 <br>
					 2. Help 2.
                     </div>
                  </div>

               </div>
            </div>

         </div>
      </div>
   
   </div>

   
 </asp:Content>
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Registration.aspx.cs" Inherits="Student.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server"></asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="register-container">
    <div class="row">
        <div class="col-md-12">
            <div class="text-center m-b-md">
                <h3>Student registration form</h3>
                <small> </small>
            </div>
            <div class="hpanel">
                <div class="panel-body">
                        <form action="#" id="loginForm">
                            <div class="row">
                            
                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelName" runat="server" Text="Name"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxName" Name="TextBoxName" runat="server" ></asp:TextBox>
                            <asp:Label ID="LabelWarningName" runat="server" Text="Field [Name] can not be empty"></asp:Label>
                            </div>

                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelSurname" runat="server" Text="Surname"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxSurname" Name="TextBoxSurname" runat="server" ></asp:TextBox>
                            <asp:Label ID="LabelWarningSurname" class="label label-warning" runat="server" Text="Field [Surname] can not be empty"></asp:Label>
                            </div>


                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelEmail" runat="server" Text="Email"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxEmail" Name="TextBoxEmail" runat="server" ></asp:TextBox>
                            <asp:Label ID="LabelWarningEmail" runat="server" Text="Field [Email] can not be empty"></asp:Label>
                            </div>

                            <div class="form-group col-lg-12">
                            <asp:Label ID="LabelMobile_phone" runat="server" Text="Mobile phone"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxMobile_phone" Name="TextBoxMobile_phone" runat="server" ></asp:TextBox>
                            </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelSuburb" runat="server" Text="Suburb"></asp:Label>
                           <asp:DropDownList class="form-control" ID="DropDownSuburb" Name="DropDownSuburb" runat="server"></asp:DropDownList>
                           </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelStreet" runat="server" Text="Street"></asp:Label>
                           <asp:TextBox class="form-control" ID="TextBoxStreet" Name="TextBoxStreet" runat="server" ></asp:TextBox>
                           </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelHouse" runat="server" Text="House"></asp:Label>
                           <asp:TextBox class="form-control" ID="TextBoxHouse" Name="TextBoxHouse" runat="server" ></asp:TextBox>
                           </div>


                  

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelGender" runat="server" Text="Gender"></asp:Label>
                           <asp:DropDownList class="form-control" ID="DropDownGender" Name="DropDownGender"  runat="server"></asp:DropDownList>
                           </div>


                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelRole" runat="server" Text="Role"></asp:Label>
                           <asp:DropDownList class="form-control" ID="DropDownRole" Name="DropDownRole"  runat="server"></asp:DropDownList>
                           </div>

                           <div class="form-group col-lg-12">
                           <asp:Label ID="LabelDate_of_birth" runat="server" Text="Date of Birth"></asp:Label>
                           <asp:TextBox class="form-control" ID="txtDate_of_birth1" runat="server" Name="txtDate_of_birth1"  ReadOnly = "true"></asp:TextBox>
                           </div>


                            <div class="form-group col-lg-6">
                            <asp:Label ID="LabelPassword" runat="server" Text="Password"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxPassword" Name="TextBoxPassword" runat="server" ></asp:TextBox>
                            <asp:Label ID="LabelWarningPassword" runat="server" Text="Field [Password] can not be empty"></asp:Label>
                            </div>

                            <div class="form-group col-lg-6">
                            <asp:Label ID="LabelRepeatPassword" runat="server" Text="Repeat Password"></asp:Label>
                            <asp:TextBox class="form-control" ID="TextBoxRepeatPassword" Name="TextBoxRepeatPassword" runat="server"></asp:TextBox>
                            </div>
                            </div>

                            <div class="text-center">
                                <asp:Button class="btn btn-success" ID="ButtonRegister" runat="server" OnClick="ButtonRegister_Click" Text="Register" />
                                <asp:Button class="btn btn-default" ID="ButtonHome" runat="server" OnClick="ButtonHome_Click" Text="Back home" />
                                <asp:Label ID="labelMessage" runat="server" Text=""></asp:Label>
                            </div>
                        </form>
                </div>
            </div>
        </div>
    </div>

</div>

</asp:Content>
